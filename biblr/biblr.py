import os
import io
import bz2
import csv
import sqlite3
from pathlib import Path

from flask import (
    Flask, request, session, g, redirect, url_for, abort,
    render_template, flash
)
import requests

from . import books

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'biblr.db'),
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('BIBLR_SETTINGS', silent=True)


def connect_db():
    '''Connects to the specific database.'''
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def get_db():
    '''Opens a new database connection if there is none yet for the
    current application context.

    '''
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    '''Closes the database again at the end of the request.'''
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


def kjv_verses():
    kjv_path = Path(app.root_path, 'kjv.tsv.bz2')
    if not kjv_path.exists():
        data = requests.get(
            'https://belhavencs.nyc3.digitaloceanspaces.com/csc211/kjv.tsv.bz2'
        ).content
        kjv_path.write_bytes(data)
        kjv_data = io.BytesIO(data)
    else:
        kjv_data = io.BytesIO(kjv_path.read_bytes())

    with bz2.open(kjv_data, 'rt') as rfp:
        columns = ['book','chapter','verse','text']
        for row in csv.DictReader(rfp, columns, delimiter='\t'):
            yield row


def insert_verses(cursor):
    rows = list(kjv_verses())
    cursor.executemany(
        'insert into kjv (book,chapter,verse,text) values'
        ' (:book,:chapter,:verse,:text)', rows
    )


def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    insert_verses(db.cursor())
    db.commit()


@app.cli.command('initdb')
def initdb_command():
    '''Initializes the database.'''
    init_db()
    app.logger.info('Initialized the database')


@app.context_processor
def inject_books():
    return {'books': books}


@app.route('/')
def index():
    db = get_db()
    return render_template('index.html')


def setup_show_verses(book, chapter, verse):
    g.book = book
    g.chapter = chapter
    g.verse = verse

    # Build the breadcrumb
    bc = [({'book':book}, books.BOOK_LUT[book])]
    if chapter is not None:
        bc.append(({'chapter': chapter, 'book': book}, chapter + 1))
    if verse is not None:
        bc.append(({'chapter': chapter, 'book': book, 'verse': verse},
                   verse + 1))
    g.breadcrumb = [(url_for('show_verses',**kw), v) for kw,v in bc]

    # Grab the verses from the database
    db = get_db()
    command = 'select book, chapter, verse, text from kjv where {}'

    where = [('book',book),('chapter',chapter),('verse',verse)]
    where_columns = [col for col,value in where if value is not None]
    where_values = [value for col,value in where if value is not None]

    where_str = ' and '.join('{}=?'.format(col) for col in where_columns)
    command = command.format(where_str)

    cur = db.execute(command, where_values)
    g.verses = cur.fetchall()



@app.route('/<book>')
@app.route('/<book>/<int:chapter>')
@app.route('/<book>/<int:chapter>/<int:verse>')
def show_verses(book, chapter=None, verse=None):
    setup_show_verses(book, chapter, verse)

    # ----------------------------------------------------------------
    # Your solution starts below this comment section
    # ----------------------------------------------------------------
    #
    # - You need to load all the comments (book, chapter, verse, and
    #   text) from the comments table
    #
    # - You then need to prepare a dictionary whose keys are a tuple
    #   (book, chapter, verse) and values are a list of comments
    #   associated with that book, chapter, and verse
    #
    # - You then need to pass this dictionary to the render_template
    #   function under the keyword "comments"
    #

    db = get_db()
    cur = db.execute('select book, chapter, verse, text from comments')
    comments = cur.fetchall()

    comments_dict = {}

    for row in comments:

        app.logger.debug('row %s', dict(row))

        key = (row['book'], row['chapter'], row['verse'])

        if key not in comments_dict:
            comments_dict[key] = []
            comments_dict[key].append(row)

    return render_template('show_verses.html', comments=comments_dict)

@app.route('/search', methods=['POST'])
def search():
    return redirect(url_for('index'))


@app.route('/add', methods=['POST'])
def add_comment():
    if not session.get('logged_in'):
        abort(401)

    # ----------------------------------------------------------------
    # Your solution starts below this comment section
    # ----------------------------------------------------------------
    #
    # - You need to get the form values for the following keys:
    #   - 'book'
    #   - 'chapter'
    #   - 'verse'
    #   - 'text'
    #
    # - 'chapter' and 'verse' need to be correctly converted to the
    #   type (and value) expected by the comments table
    #
    # - You then need to insert the (book, chapter, verse, text)
    #   values into the comments table
    #
    #
    if not session.get('logged_in'):
        abort(401)
    db = get_db()
    db.execute('insert into comments (book, chapter, verse, text) values (?, ?, ?, ?)',
                  [request.form['book'], int(request.form['chapter'])-1,
                  int(request.form['verse'])-1, request.form['text']])
    db.commit()

    flash('Your comment has been posted')
    return redirect(request.args.get('next') or
                    request.referrer or url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
            origin = request.args.get('origin')
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
            origin = request.args.get('origin')
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(request.args.get('origin'))
    origin = request.referrer
    return render_template('login.html', error=error, origin=origin)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(request.referrer)
